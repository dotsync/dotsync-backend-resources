
# dotsync-backend-resources

## Summary
This project contains the kubernetes deployment files for the database. This pod makes up the backend data layer that the server(s) interact with. This must be deployed for the system to operate successfully.

This code should not be downloaded unless for development purposes.

## Project Status
This project is in the very early stages of development and is actively being worked on.

## Installation
This code should not be downloaded unless for development purposes.
If you wish to contribute to development then follow the below instructions.

## How to setup development
In order to setup your machine for development and start tweaking the tool follows these instructions.

### Install system dependencies
Make sure your system is setup for kubernetes development with devspace.

#### k3s (optional)
This project currently uses k3s as local kubernetes cluster but you can likely use another cluster client if you're more familair.

To install k3s follow the instructions here: (k3s)[https://k3s.io/]

#### kubie (optional)
In order to manage different cluster contexts a tool like kubie can be used.

To install kubie follow the instructions here: (kubie)[https://github.com/sbstp/kubie#installation]

#### kubectl (optional)
This tool helps debugging and managing kubernetes clusters.

To install kubectl follow the instructions here: (kubectl)[https://kubernetes.io/docs/tasks/tools/]

#### k9s (optional)
This tool helps debugging and managing kubernetes deployments.
You can view and interact with your deployed resources through an interactive cli.

To install k9s follow the instructions here: (k9s)[https://k9scli.io/topics/install/]

#### devspace (required)
This project uses devspace for deployment.

To install devspace follow the instructions here: (devspace)[https://devspace.sh/docs/getting-started/installation]

### Clone the repo (or fork!)
```
git clone https://gitlab.com/dotsync/dotsync-backend-resources.git
cd dotsync-backend-resources
```

### Start coding!
Now that you have deployment source code you can start tweaking!

## Deployment
Once you have the repo cloned, a local cluster setup, and devspace installed, you can use the following instructions to develop or deploy the cluster.

### Developement
While developing code it is nice to have a shell into your cluster.

First, make sure you are in the correct context. This can be done with:
```
kubie ctx
```
Second, make sure you are in the correct namespace:
```
devspace use namespace dotsync
```
Finally, enter dev mode:
```
devspace dev
```
Now you should have a shell in the cluster!

### Deploy

When you are wanting to work on another part of the system with this version of the code you can deploy these pod(s).

First, make sure you are in the correct context. This can be done with:
```
kubie ctx
```
Second, make sure you are in the correct namespace:
```
devspace use namespace dotsync
```
Finally, deploy to the cluster:
```
devspace deploy
```
Now you should have your pod(s) spinning up.

## How to contribute
This software system is fully open source and public. In order to contribute to the development follow the below setup instructions.
Once you have your machine setup and ready pick up an issue and start working!

### Branching convention
For every ticket go ahead and make a branch off of main with the following naming convention:
- For features: feat/[ticket-number]-[ticket-name-dashed-like-this]
- For bug fixes: bug/[ticket-number]-[ticket-name-dashed-like-this]

### Merge Requests
Once you're branch is ready to be merged into main go ahead and squash your commits, update your branch off of main, and make a merge request!
Make sure to include your ticket name and number in the title. Also, write up a good description of the work you did and any important notes in the merge request description. Don't forget to link the issue with the merge request too!

## Testing
All code in this project should be unit (and integration where applicable-- see below!) tested. The CI pipeline is configured to
run automated testing for function, documentation, and formatting. Before making a merge request make sure to run your tests locally, if they fail then your merge requested won't be accepted!

### Integartion Testing
This project does not require any integration testing.
